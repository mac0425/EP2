# Nome: JULIO KENJI UEDA
# N.USP: 9298281

import util
from state import State


def validate(problem, solution):
    '''
    Return true if `solution` is a valid plan for `problem`.
    Otherwise, return false.

    OBSERVATION: you should check action applicability,
    next-state generation and if final state is indeed a goal state.
    It should give you some indication of the correctness of your planner,
    mainly for debugging purposes.
    '''
    ' YOUR CODE HERE '
    state = State(problem.init)

    for action in solution:
        pre = action.precond
        pos = action.pos_effect
        neg = action.neg_effect

        not_affected = state.difference(pos.union(neg))

        if pre <= state:
            state = state.union(pos).difference(neg)
        else:
            return False

        if not not_affected <= state:
            return False

        if not pos <= state:
            return False

        for n in neg:
            if n in state:
                return False

    if problem.goal <= state:
        return True
    return False

