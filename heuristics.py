# Nome: JULIO KENJI UEDA
# N.USP: 9298281

import util
from state import State

def h_naive(state, planning):
    return 0


def h_add(state, planning):
    '''
    Return heuristic h_add value for `state`.

    OBSERVATION: It receives `planning` object in order
    to access the applicable actions and problem information.
    '''
    ' YOUR CODE HERE '

    x = State(state)
    g = planning.problem.goal
    h = dict([(s, 0) for s in x])
    new_size = len(x)
    old_size = 0

    while new_size != old_size:
        old_size = new_size

        for a in planning.applicable(x):
            pos_effect = a.pos_effect
            x = x.union(pos_effect)

            sum_h_q = sum(h[q] for q in a.precond)

            for p in pos_effect:
                h[p] = min(h[p], 1 + sum_h_q) if p in h else 1 + sum_h_q

        new_size = len(x)

    return sum(h[p] if p in h else util.sys.maxsize for p in g)


def h_max(state, planning):
    '''
    Return heuristic h_max value for `state`.

    OBSERVATION: It receives `planning` object in order
    to access the applicable actions and problem information.
    '''
    ' YOUR CODE HERE '

    actual_state = State(state)
    goals = planning.problem.goal - actual_state

    new_size = len(actual_state)
    old_size = 0
    total_cost = 0
    while new_size != old_size and goals:
        total_cost += 1
        old_size = new_size

        for action in planning.applicable(actual_state):
            pos_effect = action.pos_effect
            actual_state = actual_state.union(pos_effect)
        new_size = len(actual_state)
        goals = goals - actual_state

    if not goals:
        return total_cost

    return util.sys.maxsize


def h_ff(state, planning):
    '''
    Return heuristic h_ff value for `state`.

    OBSERVATION: It receives `planning` object in order
    to access the applicable actions and problem information.
    '''
    util.raiseNotDefined()
    ' YOUR CODE HERE '
